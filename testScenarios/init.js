/** Test Data Init: Start */

if (_ENV == "QA") {
    _TDPath += "qa/";
} else if (_ENV == "DEV") {
    _TDPath += "dev/";
} else if (_ENV == "PROD") {
    _TDPath += "prod/";
} else {
    _TDPath += "qa/";
}

module.exports = {
    addCompetitor_TD: require("../" + _TDPath + "addCompetitor"),
    addKeywords_TD: require("../" + _TDPath + "addKeywords"),
    addSiteDetails_TD: require("../" + _TDPath + "addSiteDetails"),
    login_TD: require("../" + _TDPath + "login"),
    clickAddLink_TD: require("../" + _TDPath + "clickAddLink"),
    homePageLoad_TD: require("../" + _TDPath + "homePageLoad")
}
/** Test Data : End */