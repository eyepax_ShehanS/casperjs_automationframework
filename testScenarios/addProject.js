casper.test.begin("Adding a Project", 9, function suite(test) {

    //TestCase: Login to Sinorbis
    _testCases.login.login(casper, _init.login_TD.DataSet1.url, _init.login_TD.DataSet1.username, _init.login_TD.DataSet1.password);

    //TestCase: Click Add Link
    _testCases.verifyClickAddLink.click(casper)

    //TestCase: Home Page Load
    _testCases.verifyHomePageLoad.load(casper)

    //TestCase: AddSiteDetails-Positive
    _testCases.verifyAddSiteDetails.add(casper, _init.addCompetitor_TD.DataSet1.websiteName, _init.addCompetitor_TD.DataSet1.websiteCategory)

    //TestCase: AddKeywords-Positive
    _testCases.verifyAddKeywords.add(casper, _init.addKeywords_TD.DataSet1.webSiteKeywords)

    //TestCase: Positive Add Competitor
    _testCases.verifyAddCompetitor.add(casper, _init.addCompetitor_TD.DataSet1.competitor)

    casper.run(function() {
        test.done();
        _resetIndex();
        //_logger.log();
    });

});