casper.test.begin("Login using invalid credentials", 4, function suite(test) {

    //TestCase: Login to Sinorbis
    _testCases.failLogin.login(casper, _init.login_TD.DataSet1.url, _init.login_TD.DataSet1.username, _init.login_TD.DataSet1.password);

    casper.run(function() {
        test.done();
        _resetIndex();
    });

});