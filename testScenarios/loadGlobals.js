casper.options.viewportSize = {
    width: 1920,
    height: 1080
};
casper.options.clientScripts.push("../utilities/vendors/jquery-3.1.1.min.js");

var _INDEX = 0;

var xp = require('casper').selectXPath;
var _testCases = require("../testCases/");
var _message = require("../utilities/message");
var _snap = require("../utilities/snap");
var _generics = require("../utilities/generics.js");
var _assert = require("../utilities/assert.js");
var _logger = require("../utilities/logger.js");
var _data = require("../utilities/data.js");

/* Global Node requires */
var _js2xmlparser = require("js2xmlparser");
var _utils = require('utils');


var _BASEURL = "http://app.sinorb.is:8080/";
var _UUID = _generics.generateUUID();
var _ClIENTTIP = undefined;
var _TDPath = "/resources/data/";

_generics.getClientIp().done(function (data) {
    _ClIENTTIP = data.clientIp;
}).fail(function () {
    console.log("Ip address not found.");
});

var _ASSERTSTATE = {
	SUCCESS : 1,
	FAIL : 2,
	UNDEFINED : 3
}

/** FLAGS : Start */
var _ENV = "QA"; /**Available Flags: QA, DEV, PROD */
/** FLAGS : End */

var _init = require("../testScenarios/init.js");


function _resetIndex(){
	_INDEX = 0;
}

var _PassData = new Array();
var _FailData = new Array();