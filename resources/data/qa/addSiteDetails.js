module.exports = {
    Selectors: {
        websiteName: "//input[contains(@name, 'website')]",
        excludeSubdomains: '#exclude_subdomains'
    },
    DataSet1: {
        webSiteCategory: "Professional Services",
        websiteName: "nissanusa.com"
    }
};