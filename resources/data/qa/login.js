module.exports = {
    Selectors: {
        form: 'form',
        footerContent: '#sggFooterContent'
    },
    Assert: {
        LoginPageTitle: 'Welcome to Sinorbis',
        HomePageTitle: 'Search Ranking - accounts.google.com | Sinorbis'
    },
    InvalidAssert: {
        LoginPageTitle: 'Welcome to EyePax',
        HomePageTitle: 'Search Ranking - accounts.google.com | EyePax'
    },
    DataSet1: {
        url: "http://app.sinorb.is:8080/auth/login",
        username: "automation@sinorbis.com",
        password: "password1"
    },
    DataSet2: {
        url: "http://app.sinorb.is:8080/auth/login",
        username: "abc@sinorbis.com",
        password: "password2"
    }
};