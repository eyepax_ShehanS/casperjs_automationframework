module.exports = {
    login: require("../testCases/loginModule/login"),
    failLogin: require("../testCases/loginModule/failLogin"),
    verifyAddSiteDetails: require("../testCases/addProject/addSiteDetails/verifyAddSiteDetails"),
    verifyClickAddLink: require("../testCases/addProject/clickAddLink/verifyClickAddLink"),
    verifyAddKeywords: require("../testCases/addProject/addKeywords/verifyAddKeywords"),
    verifyHomePageLoad: require("../testCases/addProject/homePageLoad/verifyHomePageLoad"),
    verifyAddCompetitor: require("../testCases/addProject/addCompetitor/verifyAddCompetitor")
};