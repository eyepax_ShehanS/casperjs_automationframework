const homeFactory = require("../../../pages/homePage").HomePage;

exports.click = function(casper) {

	var homePage = new homeFactory();
	var moduleName = "verifyClickAddLink";

    casper.waitForSelector(xp(_init.clickAddLink_TD.Selectors.projectAddLink), function() {
    	var self = this;
        homePage.clickAdd(self);
        _snap.take(moduleName);
    });

    return casper;
};