const addProjectPageFactory = require("../../../pages/addProjectPage").AddProjectPage;

exports.add = function(casper, webSiteKeywords) {

    var appProjectPage = new addProjectPageFactory();
    var moduleName = "verifyAddKeywords";

    casper.waitForSelector(xp(_init.addKeywords_TD.Selectors.keywordInput), function() {
        var self = this;

        ////Fill the website keywords
        appProjectPage.setWebSiteKeyWordValue(self, webSiteKeywords);

        self.wait(5000, function() {
            ////Submit the site
            _snap.take(moduleName);
            appProjectPage.enableStartStepThree(self);
            appProjectPage.clickStartStepThree(self);
        });
    })
};