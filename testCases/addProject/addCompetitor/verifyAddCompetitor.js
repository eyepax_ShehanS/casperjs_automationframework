const addProjectPageFactory = require("../../../pages/addProjectPage").AddProjectPage;

exports.add = function(casper,competitor) {

    var appProjectPage = new addProjectPageFactory();
    var moduleName = "verifyAddCompetitor";

    casper.waitForSelector(xp(_init.addCompetitor_TD.Selectors.competitorInput), function() {
        var self = this;

        ////Fill the website competitor
        appProjectPage.setWebSiteCompetitorValue(self, competitor);

        self.wait(5000, function() {
            ////Submit the site
            _snap.take(moduleName);
            appProjectPage.enableStartStepFour(self);
            appProjectPage.clickStartStepFour(self);
        });
    })
};