const addProjectPageFactory = require("../../../pages/addProjectPage").AddProjectPage;

exports.load = function(casper) {

    var appProjectPage = new addProjectPageFactory();
    var moduleName = "homePageLoad";

    casper.waitForSelector(_init.homePageLoad_TD.Selectors.setupStepButton1, function() {
        var self = this;

        //checking step buttons availability
        _assert.assertExists(self, moduleName, true, true, _init.homePageLoad_TD.Selectors.setupStepButton1, 'Verify start button');
        _assert.assertExists(self, moduleName, true, true, _init.homePageLoad_TD.Selectors.setupStepButton2, 'Verify keywords button');
        _assert.assertExists(self, moduleName, true, true, _init.homePageLoad_TD.Selectors.setupStepButton3, 'Verify competitor button');
        _assert.assertExists(self, moduleName, true, true, _init.homePageLoad_TD.Selectors.setupStepButton4, 'Verify done button');

        /* Sample fail test - For testing purpose - Missing test number 8 */
        /*_assert.assert(self, moduleName, true, true, false, 'Sample test');*/

        // go to first step
        self.wait(5000, function() {
            _snap.take(moduleName, _ASSERTSTATE.UNDEFINED);
            appProjectPage.clickStartStepOne(self);
        });
    })
};