const addProjectPageFactory = require("../../../pages/addProjectPage").AddProjectPage;

exports.add = function(casper, websiteName, webSiteCategory) {

    var appProjectPage = new addProjectPageFactory();
    var moduleName = "verifyAddSiteDetails";

    casper.waitForSelector(xp(_init.addSiteDetails_TD.Selectors.websiteName), function() {
        var self = this;

        ////Fill the website name
        appProjectPage.setCompanyWebsiteNameValue(self, websiteName);

        ////Tick the exclude subdomains
        appProjectPage.clickExcludeSubdomainLabel(self);

        ////Assert tick was affected
        self.waitForSelector(_init.addSiteDetails_TD.Selectors.excludeSubdomains, function() {
            _assert.assert(self, moduleName, true, true, this.evaluate(function() {
                return document.getElementById('exclude_subdomains').checked;
            }), 'Verify exclude domain check box was checked');
        }); 

        ////Selecting the industry drop down
        appProjectPage.selectIndustryByText(self, webSiteCategory);

        self.wait(5000, function() {
            ////Navigate to the second step
            _snap.take(moduleName);
            appProjectPage.enableStartStepTwo(self);
            appProjectPage.clickStartStepTwo(self);
        });
    });
};