/*** Login ***/
const loginFactory = require("../../pages/loginPage").LoginPage;

exports.login = function(casper, url, username, password) {

    var loginPage = new loginFactory();
    var moduleName = "login";

    casper.start(url, function() {
        casper.waitForSelector(_init.login_TD.Selectors.form, function() {
            var self = this;
            _assert.assertTitle(self, moduleName, true, true, _init.login_TD.InvalidAssert.LoginPageTitle, 'Verify login page title');
            loginPage.setUsernameValue(self, username);
            loginPage.setPaswordValue(self, password);
            _snap.take(moduleName, _ASSERTSTATE.UNDEFINED);
            loginPage.enableSignIn(self);
            loginPage.clickSignIn(self);
        });
    }).then(function() {
        var self = this;
        self.waitForSelector(_init.login_TD.Selectors.footerContent, function() {
            var text = casper.fetchText(xp("//nav//h6"));
            _assert.assertTitle(self, moduleName, true, true, _init.login_TD.InvalidAssert.HomePageTitle, 'Verify home page title');
            _assert.assertExists(self, moduleName, true, true, {
                type: 'xpath',
                path: "//nav//h6"
            }, ("Verify " + text + " text exists"));
            _assert.assert(self, moduleName, true, true, (text === 'Automation'), 'Verify logged in user');
        });
    });

    return casper;
};