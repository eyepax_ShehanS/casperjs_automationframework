const componentFactory = require("../reusableComponents/components").ComponentFactory;

exports.HomePage = function(params) {
    var self = this;

    /* Initialize variables */

    ////Id selector section

    ////Xpath selector section
    var addProjectXpath = "//div[contains(@class, 'sgg-project-add-link')]//a";

    ////Class selector section
    var addProjectClass = ".sgg-project-add-link";

    /* Initialize the components */

    ////Add anchor
    var anchorElementAdd = new componentFactory({
        id: addProjectClass,
        xpath: addProjectXpath
    }).getAnchorComponent();

    /* Initilize the get and set methods for components */

    ////Click add anchor
    self.clickAdd = function(parent) {
        anchorElementAdd.click(parent);
    }
}