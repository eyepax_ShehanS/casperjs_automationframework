const componentFactory = require("../reusableComponents/components").ComponentFactory;

var loginUrl = _BASEURL + "auth/login";

exports.LoginPage = function(params) {
    var self = this;

    /* Initialize variables */

    ////Id selector section
    var usernameId = "#username",
        passwordId = "#password";

    ////Xpath selector section
    var signInXpath = "//*[@class='sgg-btn sgg-submit']";

    ////Class selector section
    var signInClass = ".sgg-submit";

    /* Initialize the components */

    ////Username text field
    var textElementUsername = new componentFactory({
        id: usernameId
    }).getTextBoxComponent();

    ////Password text field
    var textElementPassword = new componentFactory({
        id: passwordId
    }).getTextBoxComponent();

    ////SignIn button
    var buttonElementSignIn = new componentFactory({
        class: signInClass,
        xpath: signInXpath
    }).getButtonComponent();

    /* Initilize the get and set methods for components */

    ////Set value for username text field
    self.setUsernameValue = function(parent, username) {
        textElementUsername.setValue(parent, username);
    }

    ////Set value of password text field
    self.setPaswordValue = function(parent, password) {
        textElementPassword.setValue(parent, password);
    }

    ////Focus cursor on password field
    self.focusOnPasswordField = function(parent) {
        textElementPassword.focus(parent);
    }

    ////Enable sign in button if it is disabled
    self.enableSignIn = function(parent) {
        buttonElementSignIn.enable(parent);
    }

    ////Click signIn submit button
    self.clickSignIn = function(parent) {
        buttonElementSignIn.click(parent);
    }
}