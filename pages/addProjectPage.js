const componentFactory = require("../reusableComponents/components").ComponentFactory;

exports.AddProjectPage = function(params) {
    var self = this;

    /* Initialize variables */

    ////Id selector section
    var stepOneButtonId = "#setup-step-one-button",
        companyNameFieldId = "#sgg-welcome-website",
        industryDropDownId = "#sgg-welcome-select-industry",
        brandNameFieldId = "#sgg-input-brand-name",
        stepTwoButtonId = "#sgg-setup-step2-next-button",
        keyWordAreaId = "#sgg-keyword-input",
        stepThreeButtonId = "#sgg-setup-step3-next-button",
        competitorAreaId = "#sgg-competitor-input",
        stepFourButtonId = "#sgg-setup-step4-next-button";

    ////Xpath selector section
    var companyNameXpath = "//*[@id='sgg-welcome-website']",
        stepOneButtonXpath = "//*[@id='setup-step-one-button']",
        industryDropDownXpath = "//*[@id='sgg-welcome-select-industry']",
        excludeLabelXpath = "//*[@class='sgg-label']",
        brandNameFieldXpath = "//*[@id='sgg-input-brand-name']",
        stepTwoButtonXpath = "//*[@id='sgg-setup-step2-next-button']",
        keyWordAreaXpath = "//*[@id='sgg-keyword-input']",
        stepThreeButtonXpath = "//*[@id='sgg-setup-step3-next-button']",
        competitorAreaXpath = "//*[@id='sgg-competitor-input']",
        stepFourButtonXpath = "//*[@id='sgg-setup-step4-next-button']";

    ////Class selector section
    var excludeLabelClass = ".sgg-label",
        stepButtonClass = ".sgg-welcome-story-submit";

    /* Initialize the components */

    /************ Components related to navigation step one ************/
    ////Start step one button
    var buttonElementStartStepOne = new componentFactory({
        id: stepOneButtonId,
        class: stepButtonClass,
        xpath: stepOneButtonXpath
    }).getButtonComponent();

    /************ Components related to navigation step two ************/
    ////Company name text field
    var textElementCompanyWebsitename = new componentFactory({
        id: companyNameFieldId,
        xpath: companyNameXpath
    }).getTextBoxComponent();

    //// 'Exclude subdomains to restrict tracking to the domain above' label
    var labelElementExcludeSubdomain = new componentFactory({
        class: excludeLabelClass,
        xpath: excludeLabelXpath
    }).getLabelComponent();

    //// Industry drop down
    var dropDownElementIndustry = new componentFactory({
        id: industryDropDownId,
        xpath: industryDropDownXpath
    }).getDropDownComponent();

    ////Brand name text field
    var textElementBrandname = new componentFactory({
        id: brandNameFieldId,
        xpath: brandNameFieldXpath
    }).getTextBoxComponent();

    ////Start step two button
    var buttonElementStartStepTwo = new componentFactory({
        id: stepTwoButtonId,
        class: stepButtonClass,
        xpath: stepTwoButtonXpath
    }).getButtonComponent();

    /************ Components related to navigation step three ************/
    ////Website key word input text area
    var textAreaElementWebSiteKeyWordInput = new componentFactory({
        id: keyWordAreaId,
        xpath: keyWordAreaXpath
    }).getTextAreaComponent();

    ////Start step three button
    var buttonElementStartStepThree = new componentFactory({
        id: stepThreeButtonId,
        class: stepButtonClass,
        xpath: stepThreeButtonXpath
    }).getButtonComponent();

    /************ Components related to navigation step four ************/
    ////Competitor input text area
    var textAreaElementCompetitorInput = new componentFactory({
        id: competitorAreaId,
        xpath: competitorAreaXpath
    }).getTextAreaComponent();

    ////Start step four button
    var buttonElementStartStepFour = new componentFactory({
        id: stepFourButtonId,
        class: stepButtonClass,
        xpath: stepFourButtonXpath
    }).getButtonComponent();

    /* Initilize the get and set methods for components */

    /************ Methods related to navigation step one components ************/
    ////Click step one start button
    self.clickStartStepOne = function(parent) {
        buttonElementStartStepOne.click(parent);
    }

    /************ Methods related to navigation step two components ************/
    ////Set value for company website name text field
    self.setCompanyWebsiteNameValue = function(parent, companyName) {
        textElementCompanyWebsitename.setValue(parent, companyName);
    }

    ////Set value for brand text field
    self.setBrandValue = function(parent, brand) {
        textElementBrandname.setValue(parent, brand);
    }

    ////Click 'Exclude subdomains to restrict tracking to the domain above' label
    self.clickExcludeSubdomainLabel = function(parent) {
        labelElementExcludeSubdomain.click(parent);
    }

    ////Select industry drop down by text
    self.selectIndustryByText = function(parent, text) {
        dropDownElementIndustry.setOptionByText(parent, text);
    }

    ////Enable step two start button if it is disabled
    self.enableStartStepTwo = function(parent) {
        buttonElementStartStepTwo.enable(parent);
    }

    ////Click step two start button
    self.clickStartStepTwo = function(parent) {
        buttonElementStartStepTwo.click(parent);
    }

    /************ Methods related to navigation step three components ************/
    ////Set value for web site key word
    self.setWebSiteKeyWordValue = function(parent, keyWord) {
        textAreaElementWebSiteKeyWordInput.setValue(parent, keyWord);
    }

    ////Enable step three start button if it is disabled
    self.enableStartStepThree = function(parent) {
        buttonElementStartStepThree.enable(parent);
    }

    ////Click step three start button
    self.clickStartStepThree = function(parent) {
        buttonElementStartStepThree.click(parent);
    }

    /************ Methods related to navigation step four components ************/
    ////Set value for web site competitor
    self.setWebSiteCompetitorValue = function(parent, competitor) {
        textAreaElementCompetitorInput.setValue(parent, competitor);
    }

    ////Enable step four start button if it is disabled
    self.enableStartStepFour = function(parent) {
        buttonElementStartStepFour.enable(parent);
    }

    ////Click step four start button
    self.clickStartStepFour = function(parent) {
        buttonElementStartStepFour.click(parent);
    }
}