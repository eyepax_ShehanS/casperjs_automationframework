const generics = require("../../reusableComponents/generics");

exports.TextArea = function(params) {
    var id = params.id;
    var xpath = params.xpath;
    var classSelector = params.class;
    this.params = params;

    this.getValue = function(parent) {
        
    }

    this.setValue = function(parent, value) {
        parent.evaluate(function(selectroId, textValue) {
            document.querySelector(selectroId).value = textValue;
        }, id, value);
    }

    this.focus = function(parent) {
        parent.sendKeys(id, undefined, {
            keepFocus: true
        });
    }
}

exports.TextArea.prototype = generics;