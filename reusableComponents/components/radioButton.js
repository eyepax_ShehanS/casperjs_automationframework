const generics = require("../../reusableComponents/generics");

exports.RadioButton = function (params) {
    var id = params.id;
    var xpath = params.xpath;
    var classSelector = params.class;
    this.params = params;

    /**
     * Get the Radio button name
     * @parent {object}
     */
    this.getText = function (parent) {
        if (xpath !== undefined) {
            return parent.getElementInfo(xp(xpath)).text;
        } else if (id !== undefined) {
            return parent.getElementInfo(id).text;
        } else if (classSelector !== undefined) {
            return parent.getElementInfo(classSelector).text;
        }
    }

    /**
     * Radio Button click
     * @parent {object}
     */
    this.click = function (parent) {
        if (xpath !== undefined) {
            parent.click(xp(xpath));
        } else if (id !== undefined) {
            parent.click(id);
        } else if (classSelector !== undefined) {
            parent.click(classSelector);
        } else {
            console.log('Error : No selector found');
        }
    }

    /**
     * Get the attribute value of the Radio Button
     * @parent {object}
     * @attribute {text}
     */
    this.getAttributeValue = function (parent, attribute) {
        if (xpath !== undefined) {
            return parent.getElementAttribute(xp(xpath), attribute);
        } else if (id !== undefined) {
            return parent.getElementAttribute(id, attribute);
        } else if (classSelector !== undefined) {
            return parent.getElementAttribute(classSelector, attribute);
        } else {
            console.log('Error : No selector found');
        }
    }
}