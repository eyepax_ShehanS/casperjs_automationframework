const generics = require("../../reusableComponents/generics");

exports.Button = function(params) {
    var id = params.id;
    var xpath = params.xpath;
    var classSelector = params.class;
    this.params = params;

    this.getValue = function() {

    }

    this.click = function(parent) {
        if(id !== undefined){
            parent.evaluate(function(idSelector) {
                document.querySelector(idSelector).click();
            }, id);
        }else if(xpath !== undefined){
            parent.click(xp(xpath));
        }   
    }

    this.enable = function(parent) {
        if(id !== undefined){
            parent.evaluate(function(selector) {
                document.querySelector(selector).removeAttribute("disabled");
            }, id);
        }else if(classSelector !== undefined){
            parent.evaluate(function(selector) {
                document.querySelector(selector).removeAttribute("disabled");
            }, classSelector);
        }   
    }
}

exports.Button.prototype = generics;