const generics = require("../../reusableComponents/generics");

exports.DropDown = function(params) {
	var casper = params.casper;
	var id = params.id;
	var url = params.url;
	var name = params.name;
	var setToText = params.setToText;
	var setToIndex = params.setToIndex;
	this.params = params;

    this.getOptionCount = function(){
        
    }

    this.isSelected = function(){

    }

    this.getSelectedText = function(){

    }

    this.getSelectedValue = function(){

    }

    this.setOptionByValue = function(parent, value){

    }

    this.setOptionByText = function(parent, text){
        parent.evaluate(function(selectroId, textValue) {
            var querySelector = document.querySelector(selectroId);
            querySelector.options[querySelector.options.selectedIndex].text = textValue;
        }, id, text);
    }
}

exports.DropDown.prototype = generics;