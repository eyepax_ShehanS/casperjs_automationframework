const generics = require("../../reusableComponents/generics");

exports.Label = function(params) {
    var id = params.id;
    var xpath = params.xpath;
    var classSelector = params.class;
    this.params = params;

    this.getValue = function() {

    }

    this.click = function(parent) {
        if(id !== undefined){
            parent.click(id);
        }else if(xpath !== undefined){
            parent.click(xp(xpath));
        }else if (classSelector !== undefined){
            parent.click(classSelector);
        }else{
            console.log('Error : No selector found');
        }
    }
}

exports.Label.prototype = generics;