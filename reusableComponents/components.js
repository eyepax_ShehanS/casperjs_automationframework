const textBoxComponent = require("../reusableComponents/components/textBox");
const textAreaComponent = require("../reusableComponents/components/textArea");
const dropDownComponent = require("../reusableComponents/components/dropDown");
const buttonComponent = require("../reusableComponents/components/button");
const titleComponent = require("../reusableComponents/components/title");
const anchorComponent = require("../reusableComponents/components/anchor");
const labelComponent = require("../reusableComponents/components/label");

exports.ComponentFactory = function(params) {
    this.getTextBoxComponent = function() {
        var textBox = new textBoxComponent.TextBox(params);
        return textBox;
    }

    this.getTextAreaComponent = function() {
        var textArea = new textAreaComponent.TextArea(params);
        return textArea;
    }

    this.getDropDownComponent = function() {
        var dropDown = new dropDownComponent.DropDown(params);
        return dropDown;
    }

    this.getButtonComponent = function() {
        var button = new buttonComponent.Button(params);
        return button;
    }

    this.getTitleComponent = function() {
        var title = new titleComponent.Title(params);
        return title;
    }

    this.getAnchorComponent = function() {
        var anchor = new anchorComponent.Anchor(params);
        return anchor;
    }

    this.getLabelComponent = function() {
        var label = new labelComponent.Label(params);
        return label;
    }
}