exports.getErrorSnap = function(moduleName, message, exception) {
    var screenCapture = _snap.take(moduleName, _ASSERTSTATE.FAIL);
    exception.result.screenCapture = screenCapture;
    _FailData.push(exception);
    throw exception;
}


//// ************************* Overriding assertion functions ************************* ////

/*
Description - Asserts that the provided condition strictly resolves to a boolean true

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Boolnean condition - The condition to be asserted
String message - Message to display if the test is a success

Sample - assert(true, "true's true");
*/
exports.assert = function(parent, moduleName, snapOnFail, snapOnSuccess, condition, message) {
    try {
        var assertData = parent.test.assert(condition, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that an element matching the provided selector expression doesn’t exists within the remote DOM environment

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selectors can be (id, class , xpath)
String message - Message to display if the test is a success

Sample - assertDoesntExist('.taxes', 'Taxes does not exist');
*/
exports.assertDoesntExist = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, message) {
    try {
        var assertData = parent.test.assertDoesntExist(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that two values are strictly equivalent

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
mixed testValue - Content to be tested
mixed expected - Expected content
String message - Message to display if the test is a success

Sample - assertEquals(1 + 1, 2), assertEquals({a: 1, b: 2}, {a: 1, b: 2});
*/
exports.assertEquals = function(parent, moduleName, snapOnFail, snapOnSuccess, testValue, expected, message) {
    try {
        var assertData = parent.test.assertDoesntExist(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that a code evaluation in remote DOM strictly resolves to a boolean true

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Function fn - Content to be tested returned from a function
String message - Message to display if the test is a success
mixed arguments - Mixed arguments

Sample - assertEval(function() {
                return __utils__.findAll("h3.r").length >= 10;
            }, "google search for \"casperjs\" retrieves 10 or more results")

*/
exports.assertEval = function(parent, moduleName, snapOnFail, snapOnSuccess, fn, message, arguments) {
    try {
        var assertData = parent.test.assertEval(fn, _message.print(message), arguments);

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that the result of a code evaluation in remote DOM strictly equals to the expected value

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Function fn - Content to be tested returned from a function
mixed expected - Expected content
String message - Message to display if the test is a success
mixed arguments - Mixed arguments

Sample - assertEvalEquals(function() {
                return __utils__.findOne('.heaven').textContent;
            }, 'beer')
*/
exports.assertEvalEquals = function(parent, moduleName, snapOnFail, snapOnSuccess, fn, expected, message, arguments) {
    try {
        var assertData = parent.test.assertEvalEquals(fn, expected, _message.print(message), arguments);

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that a selector expression matches a given number of elements

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selectors can be (id, class , xpath)
Int count - The element count
String message - Message to display if the test is a success

Sample - assertElementCount('li', 3)
*/
exports.assertElementCount = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, count, message) {
    try {
        var assertData = parent.test.assertElementCount(selector, count, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that an element matching the provided selector expression exists in remote DOM environment

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selectors can be (id, class , xpath)
String message - Message to display if the test is a success

Sample - assertExists('#password', "Password field exists")
*/
exports.assertExists = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, message) {
    try {
        var assertData = parent.test.assertExists(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a given subject is falsy.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Mixed subject - Subject
String message - Message to display if the test is a success

*/
exports.assertFalsy = function(parent, moduleName, snapOnFail, snapOnSuccess, subject, message) {
    try {
        parent.test.assertTitle(subject, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a given form field has the provided value.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String|Object input - Input data
String expected - Expected content
String message - Message to display if the test is a success
Object options - Options to give

Sample: test.assertFieldName('q', 'plop', 'did not plop', {formSelector: 'plopper'});
*/
exports.assertFieldName = function(parent, moduleName, snapOnFail, snapOnSuccess, input, expected, message, options) {
    try {
        parent.test.assertFieldName(input, expected, _message.print(message), options);

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a given form field has the provided value given a CSS selector.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String cssSelector - css selector for the field
String expected - Expected content 
String message - Message to display if the test is a success

Sample: test.assertFieldCSS('q', 'plop', 'did not plop', 'input.plop');
*/
exports.assertFieldCSS = function(parent, moduleName, snapOnFail, snapOnSuccess, cssSelector, expected, message) {
    try {
        parent.test.assertFieldCSS(cssSelector, expected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a given form field has the provided value given a XPath selector.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String xpathSelector - xpath selector for the field
String expected - Expected content
String message - Message to display if the test is a success

Sample: test.assertFieldXPath('q', 'plop', 'did not plop', '/html/body/form[0]/input[1]');
*/
exports.assertFieldXPath = function(parent, moduleName, snapOnFail, snapOnSuccess, xpathSelector, expected, message) {
    try {
        parent.test.assertFieldXPath(xpathSelector, expected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that current HTTP status code is the same as the one passed as argument.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Number status - Status code
String message - Message to display if the test is a success

Sample: test.assertHttpStatus(200);
*/
exports.assertHttpStatus = function(parent, moduleName, snapOnFail, snapOnSuccess, status, message) {
    try {
        parent.test.assertHttpStatus(status, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a provided string matches a provided javascript RegExp pattern.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
mixed subject - subject for test
RegExp pattern - Pattern in RegExp format
String message - Message to display if the test is a success

Sample: casper.test.assertMatch('Chuck Norris', /^chuck/i, 'Chuck Norris\' first name is Chuck');
*/
exports.assertMatch = function(parent, moduleName, snapOnFail, snapOnSuccess, subject, pattern, message) {
    try {
        parent.test.assertMatch(subject, pattern, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the passed subject resolves to some falsy value.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
mixed subject - subject for test
String message - Message to display if the test is a success

Sample: test.assertNot(false, "Universe is still operational");
*/
exports.assertNot = function(parent, moduleName, snapOnFail, snapOnSuccess, subject, message) {
    try {
        parent.test.assertNot(subject, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that two values are not strictly equals.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
mixed testValue - Value of test
mixed expected - Expected content
String message - Message to display if the test is a success

Sample: test.assertNotEquals(true, "true");
*/
exports.assertNotEquals = function(parent, moduleName, snapOnFail, snapOnSuccess, testValue, expected, message) {
    try {
        parent.test.assertNotEquals(testValue, expected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the element matching the provided selector expression is not visible.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selector
String message - Message to display if the test is a success

Sample: test.assertNotVisible('.foo');
*/
exports.assertNotVisible = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, message) {
    try {
        parent.test.assertNotVisible(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the provided function called with the given parameters raises a javascript Error.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Function fn - Provide a function 
Array args - Args to above function
String message - Message to display if the test is a success

Sample: test.assertRaises(function(throwIt) { if (throwIt) { throw new Error('thrown'); }}, [false], 'Error has been raised.');
*/
exports.assertRaises = function(parent, moduleName, snapOnFail, snapOnSuccess, fn, args, message) {
    try {
        parent.test.assertRaises(fn, args, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that given text does not exist in all the elements matching the provided selector expression.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selector
String text - Text to check
String message - Message to display if the test is a success

Sample: test.assertSelectorDoesntHaveText('title', 'Yahoo!');
*/
exports.assertSelectorDoesntHaveText = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, text, message) {
    try {
        parent.test.assertSelectorDoesntHaveText(selector, text, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that given text exists in elements matching the provided selector expression.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selector
String text - Text to check
String message - Message to display if the test is a success

Sample: test.assertSelectorHasText('title', 'Google');
*/
exports.assertSelectorHasText = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, text, message) {
    try {
        parent.test.assertSelectorHasText(selector, text, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - The testFx function is executed against all loaded assets and the test passes when at least one resource matches.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Function testFx - Function
String message - Message to display if the test is a success

Sample: test.assertResourceExists(function(resource) {
            return resource.url.match('logo3w.png');
        });
*/
exports.assertResourceExists = function(parent, moduleName, snapOnFail, snapOnSuccess, testFx, message) {
    try {
        parent.test.assertResourceExists(testFx, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that body plain text content contains the given string.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String expected - Expected outcome
String message - Message to display if the test is a success

Sample: test.assertTextExists('google', 'page body contains "google"');
*/
exports.assertTextExists = function(parent, moduleName, snapOnFail, snapOnSuccess, expected, message) {
    try {
        parent.test.assertTextExists(expected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that body plain text content doesn’t contain the given string.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String unexpected - Unexpected outcome
String message - Message to display if the test is a success

Sample: test.assertTextDoesntExist('bing', 'page body does not contain "bing"');
*/
exports.assertTextDoesntExist = function(parent, moduleName, snapOnFail, snapOnSuccess, unexpected, message) {
    try {
        parent.test.assertTextDoesntExist(unexpected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that title of the remote page equals to the expected one

String expected - Expected title to be asserted
String message - Message to display if the test is a success

Sample: assertTitle('Google', 'google.fr has the correct title')
*/
exports.assertTitle = function(parent, moduleName, snapOnFail, snapOnSuccess, expected, message) {
    try {
        var assertData = parent.test.assertTitle(expected, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}


/*
Description - Asserts that title of the remote page matches the provided RegExp pattern.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
RegExp pattern - Pattern to match the Title
String message - Message to display if the test is a success

Sample: test.assertTitleMatch(/Google/, 'google.fr has a quite predictable title');
*/
exports.assertTitleMatch = function(parent, moduleName, snapOnFail, snapOnSuccess, pattern, message) {
    try {
        parent.test.assertTitleMatch(pattern, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that a given subject is truthy.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Mixed subject - Subject to Check
String message - Message to display if the test is a success

*/
exports.assertTruthy = function(parent, moduleName, snapOnFail, snapOnSuccess, subject, message) {
    try {
        parent.test.assertTruthy(subject, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the provided input is of the given type.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Mixed input - Input to assert
String type - Type
String message - Message to display if the test is a success

Sample: test.assertType(42, "number", "Okay, 42 is a number");
*/
exports.assertType = function(parent, moduleName, snapOnFail, snapOnSuccess, input, type, message) {
    try {
        parent.test.assertType(input, type, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the provided input is of the given constructor.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Mixed input - Input
Function constructor - constructor
String message - Message to display if the test is a success

Sample: test.assertInstanceOf(daisy, Cow, "Ok, daisy is a cow.");
*/
exports.assertInstanceOf = function(parent, moduleName, snapOnFail, snapOnSuccess, input, constructor, message) {
    try {
        parent.test.assertInstanceOf(input, constructor, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that the current page url matches the provided RegExp pattern.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
Regexp pattern - Pattern of Url to match
String message - Message to display if the test is a success

Sample: test.assertUrlMatch(/^http:\/\//, 'google.fr is served in http://');
*/
exports.assertUrlMatch = function(parent, moduleName, snapOnFail, snapOnSuccess, pattern, message) {
    try {
        parent.test.assertUrlMatch(pattern, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that at least one element matching the provided selector expression is visible.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selector value
String message - Message to display if the test is a success

Sample: test.assertVisible('h1');
*/
exports.assertVisible = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, message) {
    try {
        parent.test.assertVisible(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}

/*
Description - Asserts that all elements matching the provided selector expression are visible.

object parent - Parent of the test
String modulename - Name of the Module
Boolean snapOnFail - Whether to snap screeen shot on test fail or not
Boolean snapOnSuccess - Whether to snap screeen shot on test success or not
String selector - Selector value
String message - Message to display if the test is a success

Sample:  test.assertAllVisible('input[type="submit"]');
*/
exports.assertAllVisible = function(parent, moduleName, snapOnFail, snapOnSuccess, selector, message) {
    try {
        parent.test.assertAllVisible(selector, _message.print(message));

        if (snapOnSuccess) {
            var screenCapture = _snap.take(moduleName, _ASSERTSTATE.SUCCESS);
            assertData.screenCapture = screenCapture;
        }

        _PassData.push(assertData);
    } catch (e) {
        if (snapOnFail) {
            this.getErrorSnap(moduleName, message, e);
        }
    }
}