exports.log = function() {
    var fs = require('fs');

    if (_PassData.length > 0) {
        var passXml = _js2xmlparser.parse("passTestCase", _PassData),
            passPath = "../Logs/" + _UUID + "/Success/Pass.log.xml";

        fs.write(passPath, passXml, function(err) {
            if (err) {

            } else {

            }
        });
    }

    if (_FailData.length > 0) {
        var failXml = _js2xmlparser.parse("failTestCase", _FailData),
            failPath = "../Logs/" + _UUID + "/Fail/Fail.log.xml";

        fs.write(failPath, failXml, function(err) {
            if (err) {

            } else {

            }
        });
    }
}