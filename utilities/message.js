exports.print = function(message) {
    _INDEX++;

    if (message === undefined || message === null || message == '') {
        return _INDEX + "_" + "Test pass";
    } else {
        return _INDEX + "_" + message;
    }
}