/*** Main Resourse ***/
exports.getResource = function(moduleName, variableName, path) {
    //var path = '../resources/config.xml';
    var fs = require('fs');
    var xml = fs.read(path);
    var xmlDoc = undefined;

    if (fs.exists(path)) {
        if (window.DOMParser) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(xml, "text/xml");
        } else {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.loadXML(xml);
        }

        var moduleNode = xmlDoc.getElementsByTagName(moduleName);
        var childValue = moduleNode[0].getElementsByTagName(variableName)[0].firstChild.data;

        return childValue;
    }

    return null;
};

exports.getSelectorValue = function(variableName) {
    var fs = require('fs');
    var path = '../resources/selectors.xml';

    if (fs.exists(path)) {
        var xml = fs.read('../resources/selectors.xml');
        var xmlDoc = undefined;

        if (window.DOMParser) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(xml, "text/xml");
        } else {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.loadXML(xml);
        }

        var moduleNode = xmlDoc.getElementsByTagName('selectors');
        var childValue = moduleNode[0].getElementsByTagName(variableName)[0].firstChild.data;

        return childValue;
    }

    return null;
};

exports.readCSVToJson = function(CSVfileName) {
    var obj = {
        root: {
            selectors: {

            }
        }
    };

    var fs = require('fs');
    var file_h = fs.open(CSVfileName, 'r');
    var line = file_h.readLine();
    var arr = [];

    while (line) {
        arr = line.split(',');
        var key = arr[0];
        var value = arr[1];
        obj.root.selectors[key] = value;
        line = file_h.readLine();
    }

    delete obj.root.selectors['Key'];

    var json = JSON.stringify(obj);
    file_h.close();

    return json;
};

exports.jsonToXml = function(jsonString) {
    if (jsonString !== undefined && jsonString !== null) {
        var jsonxml = require("../utilities/vendors/jsontoxml.js");
        var xml = jsonxml(jsonString);
        return xml;
    } else {
        console.log('Invalid input');
    }
};

exports.csvToXmlString = function(CSVfileName) {
    var self = this;
    var json = self.readCSVToJson(CSVfileName);
    var xml = '<?xml version="1.0" encoding="UTF-8"?>' + self.jsonToXml(json);
    return xml;
};

exports.generateXmlFromCsv = function(CSVfileName) {
    var self = this;
    var path = '../resources/selectors.xml';
    var fs = require('fs');
    var xmlString = self.csvToXmlString(CSVfileName);

    if (fs.exists(path)) {
        fs.write(path, xmlString, 'w');
    }
};