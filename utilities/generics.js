const $ = require("../utilities/vendors/jquery-3.1.1.min.js");

exports.getClientIp = function() {
    var ip = undefined;
    var def = $.Deferred();

    //get our JSON and listen for done
    $.getJSON("http://jsonip.com?callback=?").done(function(data) {

        //resolve the deferred, passing it our custom data
        def.resolve({
            clientIp: data.ip
        });
    });

    //return the deferred for listening
    return def;
};

exports.generateUUID = function() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now();; //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}