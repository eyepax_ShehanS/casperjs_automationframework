exports.take = function(modulename, assertionState) {
    var loglocation = undefined;

    if (assertionState === 1) {
        loglocation = "../Logs/" + _UUID + "/Success/";
    } else if (assertionState === 2) {
        loglocation = "../Logs/" + _UUID + "/Fail/";
    } else {
        loglocation = "../Logs/" + _UUID + "/";
    }

    var date = new Date();
    var millisecond = date.getMilliseconds().toString();
    var second = date.getSeconds().toString();
    var minute = date.getMinutes().toString();
    var hour = date.getHours().toString();
    var day = date.getDate().toString();
    var month = (date.getMonth() + 1).toString();
    var year = date.getFullYear().toString();

    if (day.length === 1) {
        day = '0' + day;
    }

    if (month.length === 1) {
        month = '0' + month;
    }

    var filename = [year, month, day, hour, minute, second, millisecond].join("_");
    fileName = (filename + "-" + _INDEX + "_" + modulename + ".jpg");

    casper.capture(loglocation + fileName);

    return fileName;
}